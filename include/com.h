#ifndef _COM_H
#define _COM_H

#include <Arduino.h>
#include <freertos/queue.h>

struct rgb_t {
	uint8_t channel;
	uint8_t r, g, b;
};

class Com {
public:
	Com();
	virtual ~Com();

	bool send(struct rgb_t *d, TickType_t wait);
	bool receive(struct rgb_t *d, TickType_t wait);

private:
	QueueHandle_t m_queue;
};

#endif
