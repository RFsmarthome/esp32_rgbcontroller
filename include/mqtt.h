#ifndef _MQTT_H
#define _MQTT_H

void mqttSetup(String &hostname, String &server, String &port, String &basePath, Com *com);

bool mqttSendRGB(uint8_t channel, uint8_t r, uint8_t g, uint8_t b);

#endif
